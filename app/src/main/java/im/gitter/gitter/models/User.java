package im.gitter.gitter.models;

import android.content.ContentValues;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.Set;

public class User implements RoomListItem {

    public static final String _ID = "_id";
    public static final String USERNAME = "username";
    public static final String DISPLAY_NAME = "displayName";
    public static final String AVATAR_URL = "avatarUrl";

    @NonNull private String id;
    @NonNull private String username;
    @NonNull private String displayName;
    @NonNull private String avatarUrl;

    public User(@NonNull String id, @NonNull String username, @NonNull String displayName, @NonNull String avatarUrl) {
        this.id = id;
        this.username = username;
        this.displayName = displayName;
        this.avatarUrl = avatarUrl;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getUri() {
        return username;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    @Override
    public String getAvatarUrl(int size) {
        return avatarUrl + "s=" + size;
    }

    @Override
    public String getName() {
        return displayName;
    }

    @Override
    public boolean hasActivity() {
        return false;
    }

    @Override
    public int getUnreadCount() {
        return 0;
    }

    @Override
    public int getMentionCount() {
        return 0;
    }

    @Override
    public String getRoomId() {
        return null;
    }
    
    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(_ID, id);
        contentValues.put(USERNAME, username);
        contentValues.put(DISPLAY_NAME, displayName);
        contentValues.put(AVATAR_URL, avatarUrl);
        return contentValues;
    }

    public static User newInstance(ContentValues contentValues) {
        return new User(
                contentValues.getAsString(_ID),
                contentValues.getAsString(USERNAME),
                contentValues.getAsString(DISPLAY_NAME),
                contentValues.getAsString(AVATAR_URL)
        );
    }
}
