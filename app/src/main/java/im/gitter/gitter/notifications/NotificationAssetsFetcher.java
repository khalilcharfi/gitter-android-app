package im.gitter.gitter.notifications;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import im.gitter.gitter.LoginData;
import im.gitter.gitter.content.ModelFactory;
import im.gitter.gitter.models.Room;
import im.gitter.gitter.network.ApiRequest;
import im.gitter.gitter.network.VolleySingleton;
import im.gitter.gitter.utils.AvatarUtils;
import im.gitter.gitter.utils.MongoUtils;

public class NotificationAssetsFetcher implements Response.ErrorListener {

    private final DateTimeFormatter dateParser = ISODateTimeFormat.dateTimeParser();
    private final String roomId;
    private final Listener listener;
    private RequestQueue queue;
    private Context context;

    @Nullable private String username = null;
    @Nullable private String roomName = null;
    @Nullable private Bitmap roomAvatar = null;
    @Nullable private List<Message> messages = null;
    private boolean hasMentions = false;
    private boolean isOneToOne = false;

    public NotificationAssetsFetcher(Context context, String roomId, Listener listener) {
        this.context = context;
        this.queue = VolleySingleton.getInstance(context).getRequestQueue();
        this.roomId = roomId;
        this.listener = listener;
    }

    public void fetch() {

        String userId = new LoginData(context).getUserId();

        getUsername(userId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                username = response;
                finishIfComplete();
            }
        });

        getRoom(roomId, new Response.Listener<Room>() {
            @Override
            public void onResponse(Room response) {
                roomName = response.getUri();
                isOneToOne = response.isOneToOne();
                String avatarUrl = response.getAvatarUrl();

                getAvatar(avatarUrl, new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        roomAvatar = response;
                        finishIfComplete();
                    }
                });
            }
        });

        getOldestUnreadAndHasMention(roomId, new Response.Listener<Pair<String, Boolean>>() {
            @Override
            public void onResponse(Pair<String, Boolean> response) {
                String oldestUnread = response.first;
                hasMentions = response.second;

                if (oldestUnread == null) {
                    messages = new ArrayList<>();
                    finishIfComplete();
                    return;
                }

                getMessagesInclusive(oldestUnread, new Response.Listener<List<Message>>() {
                    @Override
                    public void onResponse(List<Message> response) {
                        messages = response;
                        finishIfComplete();
                    }
                });
            }
        });
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        queue.cancelAll(this);
        listener.onFailure(error);
    }


    private void getUsername(String userId, Response.Listener<String> listener) {
        queue.add(new ApiRequest<String>(context, "/v1/user/"+userId, listener, this) {
            @Override
            protected String parseJsonInBackground(String jsonString) throws JSONException {
                return new JSONObject(jsonString).getString("username");
            }
        }).setTag(this);
    }

    private void getRoom(String roomId, Response.Listener<Room> listener) {
        queue.add(new ApiRequest<Room>(context, "/v1/rooms/"+roomId, listener, this) {
            @Override
            protected Room parseJsonInBackground(String jsonString) throws JSONException {
                return new ModelFactory().createRoom(new JSONObject(jsonString));
            }
        }).setTag(this);
    }

    private void getAvatar(String avatarUrl, Response.Listener<Bitmap> listener) {
        final int pixelSize = context.getResources().getDimensionPixelSize(android.R.dimen.notification_large_icon_height);
        String url = new AvatarUtils(context).getAvatarWithDimen(avatarUrl, android.R.dimen.notification_large_icon_height);
        queue.add(new ImageRequest(
                url,
                listener,
                pixelSize,
                pixelSize,
                ImageView.ScaleType.CENTER,
                Bitmap.Config.ARGB_8888,
                this
        )).setTag(this);
    }

    private void getOldestUnreadAndHasMention(String roomId, Response.Listener<Pair<String, Boolean>> listener) {
        queue.add(new ApiRequest<Pair<String, Boolean>>(context, "/v1/user/me/rooms/" + roomId + "/unreadItems", listener, this) {
            @Override
            protected Pair<String, Boolean> parseJsonInBackground(String jsonString) throws JSONException {
                boolean hasMention = false;
                JSONObject jsonObject = new JSONObject(jsonString);

                String oldestUnread = null;
                Integer oldestUnreadTime = null;

                JSONArray chats = jsonObject.getJSONArray("chat");
                JSONArray mentions = jsonObject.getJSONArray("mention");

                for (int i = 0; i < chats.length(); i++) {
                    String id = chats.getString(i);
                    Integer time = MongoUtils.unixSecondsFromId(id);

                    if (oldestUnreadTime == null || oldestUnreadTime > time) {
                        oldestUnread = id;
                        oldestUnreadTime = time;
                    }
                }

                for (int i = 0; i < mentions.length(); i++) {
                    hasMention = true;
                    String id = chats.getString(i);
                    Integer time = MongoUtils.unixSecondsFromId(id);

                    if (oldestUnreadTime == null || oldestUnreadTime > time) {
                        oldestUnread = id;
                        oldestUnreadTime = time;
                    }
                }

                return new Pair<>(oldestUnread, hasMention);
            }
        }).setTag(this);
    }

    private void getMessagesInclusive(String startId, Response.Listener<List<Message>> listener) {
        String idBeforeStartId = MongoUtils.idFromUnixSeconds(MongoUtils.unixSecondsFromId(startId) - 1);

        queue.add(new ApiRequest<List<Message>>(context, "/v1/rooms/" + roomId + "/chatMessages?afterId=" + idBeforeStartId, listener, this) {
            @Override
            protected List<Message> parseJsonInBackground(String jsonString) throws JSONException {
                List<Message> messages = new ArrayList<>();
                JSONArray jsonArray = new JSONArray(jsonString);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject messageJson = jsonArray.getJSONObject(i);
                    Message message = new Message();
                    message.text = messageJson.getString("text");
                    message.timeStamp = dateParser.parseDateTime(messageJson.getString("sent")).getMillis();
                    message.sender = messageJson.getJSONObject("fromUser").getString("displayName");
                    messages.add(message);
                }

                return messages;
            }
        });
    }

    private void finishIfComplete() {
        if (username != null && roomName != null && roomAvatar != null && messages != null) {
            listener.onSuccess(username, roomName, isOneToOne, roomAvatar, messages, hasMentions);
        }
    }

    class Message {
        CharSequence text;
        long timeStamp;
        CharSequence sender;
    }

    interface Listener {
        void onSuccess(
                String username,
                String roomName,
                boolean isOneToOne,
                Bitmap roomAvatar,
                List<Message> messages,
                boolean hasMentions
        );
        void onFailure(VolleyError error);
    }
}
