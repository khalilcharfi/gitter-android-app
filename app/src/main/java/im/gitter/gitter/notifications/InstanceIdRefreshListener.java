package im.gitter.gitter.notifications;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

public class InstanceIdRefreshListener extends InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {
        // push notification token is now invalid! So we reregister:
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}
