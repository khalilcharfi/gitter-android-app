package im.gitter.gitter.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;

import com.android.volley.RequestQueue;
import com.android.volley.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import im.gitter.gitter.R;
import im.gitter.gitter.models.CreateRoomModel;
import im.gitter.gitter.models.Repo;
import im.gitter.gitter.network.ApiModelListRequest;
import im.gitter.gitter.network.VolleySingleton;

public class RoomNameDialog extends DialogFragment implements AdapterView.OnItemClickListener, TextWatcher {

    private ArrayAdapter<Repo> repoAdapter;
    private CreateRoomModel model;
    private List<Repo> allRepos = new ArrayList<>();
    private RequestQueue queue;
    private Delegate delegate;
    private Repo linkedRepo;
    private AutoCompleteTextView autoCompleteTextView;
    private TextInputLayout textViewLayout;
    private Button positiveButton;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        delegate = (Delegate) activity;
        model = delegate.getModel(this);
        repoAdapter = new ArrayAdapter<Repo>(activity, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public Filter getFilter() {
                return createRepoFilter();
            }
        };

        queue = VolleySingleton.getInstance(activity).getRequestQueue();
        queue.add(new ApiModelListRequest<Repo>(activity, "/v1/user/me/repos", createRepoRequestListener(), null) {
            @Override
            protected Repo createModel(JSONObject jsonObject) throws JSONException {
                return new Repo(
                        jsonObject.getString("uri"),
                        jsonObject.optString("description"),
                        jsonObject.getBoolean("private")
                );
            }
        }.setTag(this));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.create_room_room_name)
                .setView(R.layout.fragment_room_name_dialog)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        model.setLinkedRepo(linkedRepo);
                        model.setRoomName(autoCompleteTextView.getText().toString());
                    }
                })
                .create();
    }

    @Override
    public void onStart() {
        super.onStart();

        positiveButton = ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE);

        autoCompleteTextView = (AutoCompleteTextView) getDialog().findViewById(R.id.room_name_text);
        autoCompleteTextView.setAdapter(repoAdapter);
        autoCompleteTextView.setOnItemClickListener(this);
        autoCompleteTextView.addTextChangedListener(this);

        textViewLayout = (TextInputLayout) getDialog().findViewById(R.id.room_name_text_layout);
        textViewLayout.setErrorEnabled(true);
        textViewLayout.setHintEnabled(true);

        if (model.getRoomName() != null) {
            setLinkedRepo(model.getLinkedRepo());
            autoCompleteTextView.setText(model.getRoomName());
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (linkedRepo != null && !s.toString().equals(linkedRepo.getName())) {
            setLinkedRepo(null);
        }

        validate(s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {}

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        setLinkedRepo(repoAdapter.getItem(position));
    }

    private Response.Listener<ArrayList<Repo>> createRepoRequestListener() {
        return new Response.Listener<ArrayList<Repo>>() {
            @Override
            public void onResponse(ArrayList<Repo> response) {
                allRepos = response;
                repoAdapter.notifyDataSetChanged();
            }
        };
    }

    private Filter createRepoFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();
                List<Repo> matchingRepos = new ArrayList<>();
                for (Repo repo : allRepos) {
                    if (model.getCommunity() == null || repo.getOwner().equals(model.getCommunity().getGithubGroupName())) {

                        String lcSequence = (charSequence != null) ? charSequence.toString().toLowerCase() : "";
                        if (lcSequence.length() == 0 ||
                                repo.getOwner().toLowerCase().startsWith(lcSequence) ||
                                repo.getName().toLowerCase().startsWith(lcSequence) ||
                                repo.getUri().toLowerCase().startsWith(lcSequence)) {
                            matchingRepos.add(repo);
                        }
                    }
                }

                results.values = matchingRepos;
                results.count = matchingRepos.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                List<Repo> matchingRepos = (List<Repo>) filterResults.values;
                if (matchingRepos != null) {
                    repoAdapter.clear();
                    repoAdapter.addAll(matchingRepos);
                    repoAdapter.notifyDataSetChanged();
                }
            }
        };
    }

    private void setLinkedRepo(Repo linkedRepo) {
        this.linkedRepo = linkedRepo;
        if (linkedRepo != null) {
            textViewLayout.setHint(getString(R.string.create_room_room_name_linked_with, linkedRepo));
            autoCompleteTextView.setText(linkedRepo.getName());
        } else {
            textViewLayout.setHint(null);
        }
    }

    private void validate(String text) {
        if (text.length() < 1) {
            positiveButton.setEnabled(false);
            textViewLayout.setError(null);
        } else if (!text.matches("[a-zA-z0-9-]+")) {
            positiveButton.setEnabled(false);
            textViewLayout.setError(getString(R.string.create_room_room_name_rules));
        } else {
            positiveButton.setEnabled(true);
            textViewLayout.setError(null);
        }
    }

    public interface Delegate {
        CreateRoomModel getModel(RoomNameDialog dialog);
    }
}
